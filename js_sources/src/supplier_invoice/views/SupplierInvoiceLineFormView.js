import Mn from 'backbone.marionette';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
// import DateWidget from '../../widgets/DateWidget.js';
import SelectBusinessWidget from '../../widgets/SelectBusinessWidget.js';
import InputWidget from '../../widgets/InputWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import Radio from 'backbone.radio';
import {hideLoader, showLoader} from "../../tools";


const SupplierInvoiceLineFormView = Mn.View.extend({
    id: 'mainform-container',
    className: 'modal_overflow',
    behaviors: [FormBehavior],
    template: require('./templates/SupplierInvoiceLineFormView.mustache'),
	regions: {
		// 'date': '.date',
		'type_id': '.type_id',
		'description': '.description',
		'ht': '.ht',
		'tva': '.tva',
		'business_link': '.business_link',
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'data:modified',
    },

    childViewEvents: {
        'finish': 'onChildChange',
    },
    onBeforeSync: showLoader,
    onFormSubmitted: hideLoader,

    initialize(){
        this.channel = Radio.channel('config');
        this.type_options = this.getTypeOptions();

        this.customers_url = this.channel.request(
            'get:options',
            'company_customers_url',
        );
        this.projects_url = this.channel.request(
            'get:options',
            'company_projects_url',
        );
        this.businesses_url = this.channel.request(
            'get:options',
            'company_businesses_url',
        );
    },

    onChildChange(field_name, value) {
        this.triggerMethod('data:modified', field_name, value);
    },

    getTypeOptions() {
        return this.channel.request(
            'get:typeOptions',
            'regular'
        );
    },
    onRender(){
        var view;
        view = new InputWidget({
            value: this.model.get('description'),
            title: 'Description',
            field_name: 'description'
        });
        this.showChildView('description', view);
        let ht_editable = this.channel.request('get:form_section', 'lines:ht')['edit'];
        view = new InputWidget({
            value: this.model.get('ht'),
            title: 'Montant HT',
            field_name: 'ht',
            addon: "€",
            required: true && ht_editable,
            editable: ht_editable,
        });
        this.showChildView('ht', view);
        let tva_editable = this.channel.request('get:form_section', 'lines:tva')['edit'];
        view = new InputWidget({
            value: this.model.get('tva'),
            title: 'Montant TVA',
            field_name: 'tva',
            addon: "€",
            required: true && tva_editable,
            editable: tva_editable
        });
        this.showChildView('tva', view);


        view = new SelectWidget({
            value: this.model.get('type_id'),
            title: 'Type de dépense',
            field_name: 'type_id',
            options: this.type_options,
            placeholder: '',
            id_key: 'id',
            required: true
        });

        this.showChildView('type_id', view);
        view = new SelectBusinessWidget({
            title: 'Client concerné',
            customers_url: this.customers_url,
            projects_url: this.projects_url,
            businesses_url: this.businesses_url,
            customer_value: this.model.get('customer_id'),
            project_value: this.model.get('project_id'),
            business_value: this.model.get('business_id'),
            customer_label: this.model.get('customer_label'),
            project_label: this.model.get('project_label'),
            business_label: this.model.get('business_label'),
        });
        this.showChildView('business_link', view);
    },
    afterSerializeForm(datas){
        let modifiedDatas = _.clone(datas);
        // Hack to allow setting those fields to null.
        // Otherwise $.serializeForm skips <select> with no value selected
        modifiedDatas['customer_id'] = this.model.get('customer_id');
        modifiedDatas['project_id'] = this.model.get('project_id');
        modifiedDatas['business_id'] = this.model.get('business_id');

        return modifiedDatas;
    },
    templateContext: function(){
        return {
            title: this.getOption('title'),
            add: this.getOption('add'),
        };
    }
});
export default SupplierInvoiceLineFormView;
