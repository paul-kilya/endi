import {
    Collection
} from 'backbone';
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import _ from 'underscore';
import ButtonModel from '../../../base/models/ButtonModel';
import ButtonWidget from '../../../widgets/ButtonWidget';

import ClauseCriteriaView from "./ClauseCriteriaView";
import CommonCriteriaView from "./CommonCriteriaView";
import OneToManyCriteriaView from "./OneToManyCriteriaView";

const modelToViewMapping = {
    string: CommonCriteriaView,
    date: CommonCriteriaView,
    multidate: CommonCriteriaView,
    number: CommonCriteriaView,
    static_opt: CommonCriteriaView,
    bool: CommonCriteriaView,
    manytoone: CommonCriteriaView,
    or: ClauseCriteriaView,
    and: ClauseCriteriaView,
    onetomany: OneToManyCriteriaView,
}

const CriteriaEmptyView = Mn.View.extend({
    template: _.template("<div></div>")
})

const CriteriaCollectionView = Mn.CollectionView.extend({
    tagName: 'ul',
    emptyView: CriteriaEmptyView,
    childViewTriggers: {
        "action:clicked": "action:clicked",
        "add:click": "add:click",
        'criterion:delete': 'criterion:delete'
    },
    collectionEvents: {
        'fetch': 'render'
    },
    initialize() {
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
    },
    childView(model) {
        const view = modelToViewMapping[model.get('type')];
        view.prototype.__parentclass__ = CriteriaCollectionView;
        return view;
    },
    onRender() {
        const model = new ButtonModel({
            'label': "Ajouter un critère à ce niveau de la hiérarchie",
            'showLabel': false,
            icon: 'plus',
            event: 'add:click'
        });
        const button = new ButtonWidget({
            model: model,
            surroundingTagName: 'li'
        });
        console.log("Add the add button to the collection view");
        this.addChildView(button, this.children.length);
    }
});
export default CriteriaCollectionView