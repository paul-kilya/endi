endi.views.third\_party package
===============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.views.third_party.customer
   endi.views.third_party.supplier

Module contents
---------------

.. automodule:: endi.views.third_party
   :members:
   :undoc-members:
   :show-inheritance:
