<%doc>
    Simple page for form rendering
</%doc>

<%inherit file="${context['main_template'].uri}" />

<%block name="content">
% if 'popup' in request.params:
<h1>${title}</h1>
% endif
    ${request.layout_manager.render_panel(
        'help_message_panel',
        parent_tmpl_dict=context.kwargs
    )}
    <div class="limited_width width40">
		<div>
			${form|n}
		</div>
    </div>
</%block>
