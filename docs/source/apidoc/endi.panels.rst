endi.panels package
===================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.panels.company_index
   endi.panels.manage
   endi.panels.project
   endi.panels.supply
   endi.panels.task

Submodules
----------

endi.panels.activity module
---------------------------

.. automodule:: endi.panels.activity
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.files module
------------------------

.. automodule:: endi.panels.files
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.form module
-----------------------

.. automodule:: endi.panels.form
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.indicators module
-----------------------------

.. automodule:: endi.panels.indicators
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.menu module
-----------------------

.. automodule:: endi.panels.menu
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.navigation module
-----------------------------

.. automodule:: endi.panels.navigation
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.sidebar module
--------------------------

.. automodule:: endi.panels.sidebar
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.tabs module
-----------------------

.. automodule:: endi.panels.tabs
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.third\_party module
-------------------------------

.. automodule:: endi.panels.third_party
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.widgets module
--------------------------

.. automodule:: endi.panels.widgets
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.workshop module
---------------------------

.. automodule:: endi.panels.workshop
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.panels
   :members:
   :undoc-members:
   :show-inheritance:
