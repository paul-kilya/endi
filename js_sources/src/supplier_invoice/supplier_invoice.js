/* global AppOption; */
import $ from 'jquery';
import { applicationStartup } from '../backbone-tools.js';

import App from './components/App.js';
import Facade from './components/Facade.js';
import ValidationLimitToolbarAppClass
    from "../common/components/ValidationLimitToolbarAppClass";
import ExpenseTypeService from "../common/components/ExpenseTypeService.js";
import StatusHistoryApp from "../common/components/StatusHistoryApp.js";

const ToolbarApp = new ValidationLimitToolbarAppClass();

$(function(){
    applicationStartup(
        AppOption, App, Facade,
        {
            actionsApp: ToolbarApp,
            statusHistoryApp: StatusHistoryApp,
            customServices: [ExpenseTypeService]
        },
    );
});
