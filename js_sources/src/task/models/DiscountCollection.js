import Bb from 'backbone';
import DiscountModel from './DiscountModel.js';
import Radio from 'backbone.radio';
import { ajax_call } from '../../tools.js';


const DiscountCollection = Bb.Collection.extend({
    model: DiscountModel,
    initialize: function(options) {
        this.on('saved', this.channelCall);
        this.on('destroyed', this.channelCall);
    },
    channelCall: function(){
        var channel = Radio.channel('facade');
        channel.trigger('changed:discount');
    },
    ht: function(){
        var result = 0;
        this.each(function(model){
            result += model.ht();
        });
        return result;
    },
    tvaParts: function(){
        var result = {};
        this.each(function(model){
            var tva_amount = model.tva();
            var tva = model.get('tva');
            if (tva in result){
                tva_amount += result[tva];
            }
            result[tva] = tva_amount;
        });
        return result;
    },
    ttc: function(){
        var result = 0;
        this.each(function(model){
            result += model.ttc();
        });
        return result;
    },
    afterInsertPercent(){
        let this_ = this
        this.fetch().then(function(){this_.trigger('saved')});
    },
    insert_percent: function(model){
        /*
         * Call the server to generate percent based Discounts
         * :param obj model: A DiscountPercentModel instance
         */
        var serverRequest = ajax_call(
            this.url + '?action=insert_percent',
            model.toJSON(),
            'POST'
        );
        serverRequest.then(this.afterInsertPercent.bind(this));
    },
    validate: function(){
        var result = {};
        this.each(function(model){
            var res = model.validate();
            if (res){
                _.extend(result, res);
            }
        });
        return result;
    }
});
export default DiscountCollection;
