/*
 * File Name :  PriceStudyModel
 */
import Radio from 'backbone.radio';

import {
    formatAmount
} from 'math.js';
import BaseModel from 'base/models/BaseModel.js';

const PriceStudyModel = BaseModel.extend({
    props: [
        "id",
        "ht",
        "general_overhead",
        "margin_rate",
        "mask_hours",
        "total_ht",
        "total_ttc",
        "tva_parts",
        "total_ht_before_discount",
    ],
    validation: {
        label: {
            required: true,
            msg: "Ce champ est obligatoire"
        }
    },
    initialize: function () {
        BaseModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('priceStudyFacade');
        this.config = Radio.channel('config');
        this.user_prefs = Radio.channel('user_prefs');
    },
    tva_labels: function () {
        var values = [];
        var this_ = this;
        const tva_options = this.config.request(
            'get:options',
            'tvas'
        );

        _.each(
            this.get('tva_parts'),
            function (item, tva_id) {
                let tva = _.findWhere(tva_options, {
                    id: parseInt(tva_id)
                });
                values.push({
                    'value': formatAmount(item),
                    label: tva.label
                });
            });
        return values;
    },
    ht_label() {
        return formatAmount(this.get('total_ht'));
    },
    ht_before_discounts_label() {
        return formatAmount(this.get('total_ht_before_discount'));
    },
    ttc_label() {
        return formatAmount(this.get('total_ttc'));
    }
});
export default PriceStudyModel;