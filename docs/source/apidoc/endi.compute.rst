endi.compute package
====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.compute.price_study
   endi.compute.sage
   endi.compute.sage_generation_expert
   endi.compute.sale_product
   endi.compute.task

Submodules
----------

endi.compute.base\_line module
------------------------------

.. automodule:: endi.compute.base_line
   :members:
   :undoc-members:
   :show-inheritance:

endi.compute.expense module
---------------------------

.. automodule:: endi.compute.expense
   :members:
   :undoc-members:
   :show-inheritance:

endi.compute.math\_utils module
-------------------------------

.. automodule:: endi.compute.math_utils
   :members:
   :undoc-members:
   :show-inheritance:

endi.compute.parser module
--------------------------

.. automodule:: endi.compute.parser
   :members:
   :undoc-members:
   :show-inheritance:

endi.compute.supplier\_invoice module
-------------------------------------

.. automodule:: endi.compute.supplier_invoice
   :members:
   :undoc-members:
   :show-inheritance:

endi.compute.supplier\_order module
-----------------------------------

.. automodule:: endi.compute.supplier_order
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.compute
   :members:
   :undoc-members:
   :show-inheritance:
