État de gestion et Bulletins de Salaire au format PDF
======================================================

États de gestion
-----------------

enDI permet de présenter des états de gestion aux entrepreneurs

    - Fichiers PDF accessible direcement sur disque (ancienne méthode)

    - État de gestion compilé par enDI

Bulletin de salaire
--------------------

Les bulletins de salaire déposés sur disque au format PDF sont présentés dans
enDI.


PDF sur disque
---------------

Le chemin sur disque est configuré dans le fichier .ini


.. code-block:: ini

    [app:endi]
    ...
    endi.ftpdir=/path/documents/

Les documents doivent alors être accessibles suivant la nomenclature suivante

    - /path/documents/tresorerie/

    - /path/documents/resultat/

    - /path/documents/salaire/


Sous une structure annee/mois. Par exemple /path/documents/salaire/2020/10/

Les fichiers PDF sont alors nommés sous la forme :

    <code_analytique_enseigne>_autre_texte.pdf


Le fichier /path/documents/salaire/2020/10/ABCD_Jean_Dupont.pdf sera proposé aux
entrepreneurs de l'enseigne avec le compte analytique ABCD

.. warning:: Les droits d'accès au fichier sont dépendant de son nom.

