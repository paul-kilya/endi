endi.panels.manage package
==========================

Submodules
----------

endi.panels.manage.activities module
------------------------------------

.. automodule:: endi.panels.manage.activities
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.manage.expenses module
----------------------------------

.. automodule:: endi.panels.manage.expenses
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.manage.suppliers module
-----------------------------------

.. automodule:: endi.panels.manage.suppliers
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.manage.tasks module
-------------------------------

.. automodule:: endi.panels.manage.tasks
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.panels.manage
   :members:
   :undoc-members:
   :show-inheritance:
