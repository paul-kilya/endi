/* global AppOption; */
import $ from 'jquery';

import { applicationStartup } from '../backbone-tools.js';
import StatusHistoryApp from "../common/components/StatusHistoryApp";
import Mn from "backbone.marionette";
import FacadeModelApiMixin from "./components/FacadeModelApiMixin";
import {ajax_call, hideLoader} from "../tools";
import StatusLogEntryCollection
    from "../common/models/StatusLogEntryCollection";


/** View to show/manage StatusLogEntries/Mémos of a Node on a mako view
 *
 * Features :
 *
 * - display the latest or pinned entries
 * - on click show full thread in a popin
 * - allow add/edit/delete of allowed memos
 *
 * Kindof facade stub : shows Backbone/Marionette view inside a mako view.
 *
 * Allows to re-use common mechanic (regarding status log entries), between JS
 * and non-JS views. Long term plan is that those views will become JS-based anyway.
 */
const FacadeClass = Mn.Object.extend(FacadeModelApiMixin).extend({
    setup(options){
        this.app_options = _.clone(options);
    },
    initialize(options) {
        this.models = {};
        this.collections = {};
    },
    setupModels(context_datas) {
        this.collections['status_history'] = new StatusLogEntryCollection(
            context_datas['status_history']
        );
    },
    start() {
        const url = this.app_options.context_url;
        return ajax_call(url).then(this.setupModels.bind(this));
    },
    syncModel() {},
});


$(function(){
    // We have no main app (python-handled)
    applicationStartup(
        AppOption, null, new FacadeClass(),
        {statusHistoryApp: StatusHistoryApp}
    );
    hideLoader();
});
