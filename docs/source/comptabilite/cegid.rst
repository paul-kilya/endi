Export des écritures au format Cegid
=======================================

enDI permet de configurer l'export des écritures au format compatible avec ce
qui est attendu par Cegid.

Afin de configurer enDI pour utiliser ces modules, la configuration suivante
doit être ajoutée dans la section "[app:endi]" du fichier .ini

.. code-block::

    endi.services.treasury_invoice_writer=endi.export.cegid.InvoiceWriter

    endi.services.treasury_payment_writer=endi.export.cegid.PaymentWriter

    endi.services.treasury_expense_writer=endi.export.cegid.ExpenseWriter

    endi.services.treasury_supplier_invoice_writer=endi.export.cegid.SupplierInvoiceWriter

    endi.services.treasury_supplier_payment_writer=endi.export.cegid.SupplierPaymentWriter

    endi.services.treasury_expense_payment_writer=endi.export.cegid.ExpensePaymentWriter
