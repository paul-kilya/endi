<%inherit file="${context['main_template'].uri}" />

<%block name="content">
% for version in release_notes:
    <%
    if version['is_last_version']:
        css_classes = "in"
        expanded = "true"
    else:
        css_classes = ""
        expanded = "false"
    %>
    <div class="version collapsible ${css_classes}">
        <div class="separate_block">
            <h2 class="title collapse_title">
                <a data-target="#v${version['version_code']}-content" data-toggle="collapse" aria-expanded="${expanded}" aria-controls="v${version['version_code']}-content">
                    ${api.icon("chevron-down", "arrow")}
                    Version ${version["version"]} <small>${version["date"]}</small>
                </a>
            </h2>
            <div class="collapse_content">
                <div id="v${version['version_code']}-content" class="content collapse ${css_classes}" aria-expanded="${expanded}">
                % for notes in (version["enhancements"], version["bugfixs"]):
                    % if len(notes) > 0:
                        % if notes[0]["category"] == "enhancement":
                            <h3>
                                <span class="icon">${api.icon("star")}</span>
                                Évolutions
                            </h3>
                        % else:
                            <h3>
                                <span class="icon">${api.icon("wrench")}</span>
                                Corrections
                            </h3>
                        % endif
                            <ul class="version_notes">
                            % for note in notes:
                                <li>
                                    <h4>
                                        ${note["title"]}
                                        % for sponsor in note["sponsors"]:
                                            <span class="icon tag neutral">${api.icon("user")} ${sponsor}</span>
                                        % endfor
                                    </h4>
                                    % for description in note["description"]:
                                        <p class="note_description">${description}</p>
                                    % endfor
                                    % if "link" in note:
                                        <a class="note_link" href="${note['link']['url']}" target="_blank">${note["link"]['title']}</a>
                                    % endif
                                </li>
                            % endfor
                            </ul>
                    % endif
                % endfor
                </div>
            </div>
        </div><!-- div.separate_block -->
    </div>
% endfor
</%block>
