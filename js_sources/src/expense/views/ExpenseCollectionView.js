import Mn from 'backbone.marionette';
import ExpenseView from './ExpenseView.js';
import ExpenseEmptyView from './ExpenseEmptyView.js';

const ExpenseCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    // Bubble up child view events
    childViewTriggers: {
        'edit': 'line:edit',
        'delete': 'line:delete',
        'bookmark': 'bookmark:add',
        'duplicate': 'line:duplicate',
    },
    childView: ExpenseView,
    emptyView: ExpenseEmptyView,
    isAchatView() {
        return this.getOption('category').value == 2;
    },
    emptyViewOptions(){
        return {
            colspan: this.isAchatView() ? 8 : 7,
            edit: this.getOption('edit'),
        };
    },
    childViewOptions(){
        return {
            edit: this.getOption('edit'),
            can_validate_attachments: this.getOption('can_validate_attachments'),
        };
    },

    viewFilter(view, index, children) {
        if (view.model.get('category') == this.getOption('category').value){
            return true;
        } else {
            return false;
        }
    }
});
export default ExpenseCollectionView;
