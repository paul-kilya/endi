#!/bin/bash
echo "Resetting db"
if [ "$1" == '-f' ]
then
    echo "Forcing"
    MYSQLCMD='mysql -u root'
    if [ "$2" != "" ]
    then
        DBNAME=$2
    else
        DBNAME='endi'
    fi
    DBUSER='endi@localhost'
    DBPASS='endi'
elif [ "$1" == "docker" ]
then
    echo "Docker mode"
    MYSQLCMD='mysql -uroot -P 3308 -h 127.0.0.1 -pmariadb'
    DBUSER='endi@%'
    DBPASS='endi'
    DBNAME='endi'
else
    echo "Enter the mysql command line needed to have root access (default: 'mysql -u root')"
    read MYSQLCMD
    if [ "$MYSQLCMD" == '' ]
    then
        MYSQLCMD='mysql -u root'
    fi
    echo "Enter the database name (default : 'endi')"
    read DBNAME
    if [ "$DBNAME" == '' ]
    then
        DBNAME='endi'
    fi
    echo "Enter the database user (default : 'endi@localhost')"
    read DBUSER
    if [ "$DBUSER" == '' ]
    then
        DBUSER='endi@localhost'
    fi
    echo "Enter the database user password (default : 'endi')"
    read DBPASS
    if [ "$DBPASS" == '' ]
    then
        DBPASS='endi'
    fi

fi

if [ "$1" == "docker" ]
then
    echo "Stop endi container"
    # Le service pserve qui tourne en attendant bloque parfois la connexion
    docker stop endi
fi
echo "Deleting database ${DBNAME}"
echo "drop database ${DBNAME};" | ${MYSQLCMD}
echo "create database ${DBNAME};" | ${MYSQLCMD}
echo "grant all privileges on ${DBNAME}.* to '${DBUSER}' identified by '${DBPASS}';" | ${MYSQLCMD}
echo "flush privileges;" | ${MYSQLCMD}
echo "Database reseted"
