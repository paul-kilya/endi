import getFormConfigStore from './formConfig'
import getModelStore from './modelStore.ts'

export const useTaskStore = getModelStore('task')
export const useTaskConfigStore = getFormConfigStore('task')
