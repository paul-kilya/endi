endi.tests.views package
========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.tests.views.accounting
   endi.tests.views.admin
   endi.tests.views.estimations
   endi.tests.views.expenses
   endi.tests.views.internal_invoicing
   endi.tests.views.invoices
   endi.tests.views.price_study
   endi.tests.views.progress_invoicing
   endi.tests.views.project
   endi.tests.views.sale_product
   endi.tests.views.statistics
   endi.tests.views.supply
   endi.tests.views.task
   endi.tests.views.third_party
   endi.tests.views.user
   endi.tests.views.userdatas

Submodules
----------

endi.tests.views.test\_activity module
--------------------------------------

.. automodule:: endi.tests.views.test_activity
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_auth module
----------------------------------

.. automodule:: endi.tests.views.test_auth
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_base module
----------------------------------

.. automodule:: endi.tests.views.test_base
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_commercial module
----------------------------------------

.. automodule:: endi.tests.views.test_commercial
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_company module
-------------------------------------

.. automodule:: endi.tests.views.test_company
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_csv\_import module
-----------------------------------------

.. automodule:: endi.tests.views.test_csv_import
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_forms\_holiday module
--------------------------------------------

.. automodule:: endi.tests.views.test_forms_holiday
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_forms\_project module
--------------------------------------------

.. automodule:: endi.tests.views.test_forms_project
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_forms\_utils module
------------------------------------------

.. automodule:: endi.tests.views.test_forms_utils
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_forms\_validator module
----------------------------------------------

.. automodule:: endi.tests.views.test_forms_validator
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_holiday module
-------------------------------------

.. automodule:: endi.tests.views.test_holiday
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_treasury\_files module
---------------------------------------------

.. automodule:: endi.tests.views.test_treasury_files
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.test\_workshop module
--------------------------------------

.. automodule:: endi.tests.views.test_workshop
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.views
   :members:
   :undoc-members:
   :show-inheritance:
