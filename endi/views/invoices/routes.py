import os

API_COMPANY_COLLECTION_ROUTE = "/api/v1/companies/{id}/invoices"
API_INVOICE_ADD_ROUTE = os.path.join(API_COMPANY_COLLECTION_ROUTE, "add")

API_INVOICE_COLLECTION_ROUTE = "/api/v1/invoices"
API_INVOICE_ITEM_ROUTE = os.path.join(API_INVOICE_COLLECTION_ROUTE, "{id}")
API_CINV_COLLECTION_ROUTE = "/api/v1/cancelinvoices"
API_CINV_ITEM_ROUTE = os.path.join(API_CINV_COLLECTION_ROUTE, "{id}")

INVOICE_COLLECTION_ROUTE = "/invoices"
INVOICE_ITEM_ROUTE = "/invoices/{id}"
CINV_ITEM_ROUTE = "/cancelinvoices/{id}"


def includeme(config):
    for route in API_COMPANY_COLLECTION_ROUTE, API_INVOICE_ADD_ROUTE:
        config.add_route(route, route, traverse="/companies/{id}")

    for route in (
        API_CINV_COLLECTION_ROUTE,
        API_INVOICE_COLLECTION_ROUTE,
        INVOICE_COLLECTION_ROUTE,
    ):
        config.add_route(route, route)

    for route in (
        API_INVOICE_ITEM_ROUTE,
        API_CINV_ITEM_ROUTE,
        INVOICE_ITEM_ROUTE,
        CINV_ITEM_ROUTE,
    ):
        # On assure qu'on matche la route qui finit par un id et pas id.html
        # par exemple
        pattern = r"{}".format(route.replace("id", r"id:\d+"))
        config.add_route(route, pattern, traverse="/tasks/{id}")
