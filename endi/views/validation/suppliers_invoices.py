import logging
import colander

from endi_base.models.base import DBSESSION
from endi.forms.validation.suppliers_invoices import get_list_schema
from endi.models.company import Company
from endi.models.supply import SupplierInvoice
from endi.models.third_party.supplier import Supplier
from endi.views import BaseListView


logger = logging.getLogger(__name__)


class SuppliersInvoicesValidationView(BaseListView):
    title = "Factures fournisseurs en attente de validation"
    schema = get_list_schema()
    sort_columns = dict(
        status_date=SupplierInvoice.status_date,
        company=Company.name,
        date=SupplierInvoice.date,
        remote_invoice_number=SupplierInvoice.remote_invoice_number,
        supplier=Supplier.name,
        cae_percentage=SupplierInvoice.cae_percentage,
    )
    add_template_vars = ("title",)
    default_sort = "status_date"
    default_direction = "desc"

    def query(self):
        query = DBSESSION().query(SupplierInvoice)
        query = query.outerjoin(SupplierInvoice.company)
        query = query.outerjoin(SupplierInvoice.supplier)
        query = query.filter(SupplierInvoice.status == "wait")
        return query

    def filter_company(self, query, appstruct):
        company_id = appstruct.get("company_id")
        if company_id and company_id not in ("", -1, colander.null):
            query = query.filter(
                SupplierInvoice.company_id == company_id,
            )
        return query

    def filter_follower(self, query, appstruct):
        follower_id = appstruct.get("follower_id")
        if follower_id not in (None, colander.null):
            query = query.filter(Company.follower_id == follower_id)
        return query

    def filter_supplier(self, query, appstruct):
        supplier_id = appstruct.get("supplier_id")
        if supplier_id and supplier_id not in ("", -1, colander.null):
            query = query.filter(
                SupplierInvoice.supplier_id == supplier_id,
            )
        return query

    def filter_doctype(self, query, appstruct):
        type_ = appstruct.get("doctype")
        if type_ in (
            "supplier_invoice",
            "internalsupplier_invoice",
        ):
            query = query.filter(SupplierInvoice.type_ == type_)
        return query


def includeme(config):
    config.add_route("validation_suppliers_invoices", "validation/suppliers_invoices")
    config.add_view(
        SuppliersInvoicesValidationView,
        route_name="validation_suppliers_invoices",
        renderer="validation/suppliers_invoices.mako",
        permission="admin.supplier_invoice",
    )
    config.add_admin_menu(
        parent="validation",
        order=4,
        label="Factures fournisseurs",
        href="/validation/suppliers_invoices",
        permission="admin.supplier_invoice",
    )
