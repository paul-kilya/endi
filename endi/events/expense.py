"""
Events used while handling expenses :
    Send email

"""
import datetime
import logging

from endi_base.mail import format_link
from endi_base.utils.date import (
    format_date,
)
from endi.utils.notification import AbstractNotification, notify
from endi.utils.strings import (
    format_account,
)

logger = logging.getLogger(__name__)

# Events for which a mail will be sended
EVENTS = {
    "valid": "validée",
    "invalid": "invalidée",
    "paid": "partiellement payée",
    "resulted": "payée",
}

BODY_TMPL = """\
Bonjour {owner},

La note de dépenses de {owner} pour la période {date} a été {status_verb}.

Vous pouvez la consulter ici :
{addr}

Commentaires associés au document :
    {comment}"""


EXPENSE_NOTIFY_STATUS = dict(
    (
        ("invalid", "Invalidée par {0} le {1}"),
        ("valid", "Validée par {0} le {1}"),
        ("paid", "Paiement partiel notifié par {0} le {1}"),
        ("resulted", "Paiement notifié par {0} le {1}"),
    )
)


def _format_expense_notification(event) -> str:
    """
    Return a formatted string for expense status notification
    """
    status_str = EXPENSE_NOTIFY_STATUS.get(event.status)
    account_label = format_account(event.request.user)
    date_label = format_date(datetime.date.today())

    if status_str is not None:
        return status_str.format(account_label, date_label)
    else:
        return ""


def get_title(event) -> str:
    """
    return the title of the notification
    """
    subject = "Notes de dépense de {0} : {1}".format(
        format_account(event.node.user),
        _format_expense_notification(event),
    )
    return subject


def _get_status_verb(status) -> str:
    """
    Return the verb associated to the current status
    """
    return EVENTS.get(status, "")


def _find_comment(event) -> str:
    """
    Collect the latest comment for the current node
    """
    if event.comment:
        return event.comment
    else:
        logger.debug("Trying to find expense status")
        status_history = event.node.statuses
        if len(status_history) > 0:
            return status_history[0].comment
        else:
            return "Aucun"


def get_body(event) -> str:
    """
    return the body of the notification
    """
    owner = format_account(event.node.user)
    date = "{0}/{1}".format(event.node.month, event.node.year)
    status_verb = _get_status_verb(event.status)
    addr = event.request.route_url("/expenses/{id}", id=event.node.id)
    addr = format_link(event.get_settings(), addr)
    return BODY_TMPL.format(
        owner=owner,
        addr=addr,
        date=date,
        status_verb=status_verb,
        comment=_find_comment(event),
    )


def get_notification(event) -> AbstractNotification:
    return AbstractNotification(
        key=f"expense:status:{event.status}",
        title=get_title(event),
        body=get_body(event),
    )


def notify_expense_status_changed(event: "StatusChanged"):
    """
    Fire notification for expense status changed
    """
    if event.status not in list(EVENTS.keys()):
        return
    notify(event.request, get_notification(event), user_ids=[event.node.user.id])
