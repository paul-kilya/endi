import Mn from 'backbone.marionette';
const template = require('./templates/EntryEmptyView.mustache');
const EntryEmptyView = Mn.View.extend({
    template: template,
});
export default EntryEmptyView