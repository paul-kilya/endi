endi.views.admin package
========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.views.admin.accompagnement
   endi.views.admin.accounting
   endi.views.admin.expense
   endi.views.admin.main
   endi.views.admin.sale
   endi.views.admin.supplier
   endi.views.admin.userdatas

Submodules
----------

endi.views.admin.layout module
------------------------------

.. automodule:: endi.views.admin.layout
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.admin.tools module
-----------------------------

.. automodule:: endi.views.admin.tools
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.views.admin
   :members:
   :undoc-members:
   :show-inheritance:
