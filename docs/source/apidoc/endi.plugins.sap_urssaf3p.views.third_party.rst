endi.plugins.sap\_urssaf3p.views.third\_party package
=====================================================

Submodules
----------

endi.plugins.sap\_urssaf3p.views.third\_party.customer module
-------------------------------------------------------------

.. automodule:: endi.plugins.sap_urssaf3p.views.third_party.customer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.plugins.sap_urssaf3p.views.third_party
   :members:
   :undoc-members:
   :show-inheritance:
